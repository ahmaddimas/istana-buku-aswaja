<?php

namespace App\Model;

use App\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductReview extends Model
{
    use SoftDeletes;

    protected $table = "iba_product_review";

    protected $fillable = [
        "id", "id_product", "rating", "review", "created_at", "updated_at"
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function product() {
        return $this->belongsTo(Product::class, Constants::FIELD_PRODUCT_ID);
    }
}
