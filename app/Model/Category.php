<?php

namespace App\Model;

use App\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = "iba_category";

    protected $fillable = [
        "id", "id_product_group", "name", "created_at", "updated_at"
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function group()
    {
        return $this->belongsTo(ProductGroup::class, Constants::FIELD_PRODUCT_GROUP_ID);
    }

    public function product()
    {
        return $this->belongsToMany(Product::class, ProductCategory::class, Constants::FIELD_CATEGORY_ID, Constants::FIELD_PRODUCT_ID);
    }
}
