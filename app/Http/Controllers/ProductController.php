<?php
/**
 * Created by ahmad.
 * Date: 10/25/18
 * Time: 5:07 PM
 */

namespace App\Http\Controllers;


use App\Exceptions\Verification;
use App\Model\Product;
use App\Model\ProductCategory;
use App\Model\ProductComponent;
use App\Model\ProductGroup;
use App\Utilities\Constants;
use App\Utilities\Response;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\Exceptions\Exception;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function getProduct() {
        try {
            $rows = Product::all();
            $result = array();

            if ($rows) {
                $result = $rows->map(function ($item) {
                    $temp = array();
                    foreach ($item->toArray() as $key => $value) {
                        if ($key !== Constants::FIELD_PRODUCT_GROUP_ID) {
                            $temp[$key] = $key == Constants::FIELD_PRICE ? number_format($value, 0, ',', '.') : $value;
                        }
                    }
                    $temp['group'] = $item->group->toArray();
                    $temp['category'] = $item->category->toArray();
                    return $temp;
                });
            }
            return Response::success($result);
        } catch (Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function getGroup() {
        try {
            return Response::success(ProductGroup::all());
        } catch (Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function getTableProduct() {
        try {
            $rows = Product::all();
            $result = array();

            if ($rows) {
                $result = $rows->map(function ($item) {
                    $temp = array();
                    foreach ($item->toArray() as $key => $value) {
                        if ($key !== Constants::FIELD_PRODUCT_GROUP_ID) {
                            $temp[$key] = $key == Constants::FIELD_PRICE ? number_format($value, 0, ',', '.') : $value;
                        }
                    }
                    $temp['group'] = $item->group->toArray();
                    $temp['category'] = $item->category->toArray();
                    return $temp;
                });
            }
            return DataTables::collection($result)->make(true);
        } catch (Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function getTableGroup() {
        try {
            return DataTables::eloquent(ProductGroup::query())->make(true);
        } catch (Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function addProduct() {
        try {
            $product = array();
            $component = array();
            $category = array();
            foreach (Input::get() as $key => $value) {
                if ($key == Constants::ID) continue;
                Verification::check($key);
                if ($key == Constants::COMPONENT_NAME || $key == Constants::COMPONENT_DESC) {
                    foreach ($value as $index => $item) {
                        $component[$index][$key] = $item;
                    }
                    continue;
                }
                if ($key == Constants::FIELD_CATEGORY_ID) {
                    $category[Constants::FIELD_CATEGORY_ID] = $value;
                    continue;
                }
                $product[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $product[Constants::FIELD_CREATED_AT] = $time;
            $product[Constants::FIELD_UPDATED_AT] = $time;
            $product = Product::create($product);

            $category[Constants::FIELD_PRODUCT_ID] = $product->id;
            $category[Constants::FIELD_CREATED_AT] = $time;
            $category[Constants::FIELD_UPDATED_AT] = $time;
            ProductCategory::create($category);

            if ($component) {
                foreach ($component as $index => $item) {
                    unset($component[$index]);
                    $component[$index][Constants::FIELD_PRODUCT_ID] = $product->id;
                    $component[$index][Constants::FIELD_NAME] = $item[Constants::COMPONENT_NAME];
                    $component[$index][Constants::FIELD_DESCRIPTION] = $item[Constants::COMPONENT_DESC];
                    $component[$index][Constants::FIELD_CREATED_AT] = $time;
                    $component[$index][Constants::FIELD_UPDATED_AT] = $time;
                }
                ProductComponent::insert($component);
            }
            return Response::success("Successfully add product");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILED);
        }
    }

    public function editProduct() {
        try {
            $product = array();
            $component = array();
            foreach (Input::get() as $key => $value) {
                if ($key == Constants::COMPONENT_NAME || $key == Constants::COMPONENT_DESC) {
                    foreach ($value as $index => $item) {
                        $component[$index][$key] = $item;
                    }
                }
                Verification::check($key);
                if ($key == Constants::ID) continue;
                $product[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $product[Constants::FIELD_UPDATED_AT] = $time;
            Product::find(Input::get(Constants::ID))->update($product);

            if ($component) {
                foreach ($component as $index => $item) {
                    unset($component[$index]);
                    $component[$index][Constants::FIELD_PRODUCT_ID] = Input::get(Constants::ID);
                    $component[$index][Constants::FIELD_NAME] = $item[Constants::COMPONENT_NAME];
                    $component[$index][Constants::FIELD_DESCRIPTION] = $item[Constants::COMPONENT_DESC];
                    $component[$index][Constants::FIELD_CREATED_AT] = $time;
                    $component[$index][Constants::FIELD_UPDATED_AT] = $time;
                }
                ProductComponent::where(Constants::FIELD_PRODUCT_ID, Input::get(Constants::ID))->forceDelete();
                ProductComponent::insert($component);
            }
            return Response::success("Successfully update product");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function deleteProduct() {
        try {
            Verification::check(Constants::FIELD_PRODUCT_ID);
            $product_id = Input::get(Constants::FIELD_PRODUCT_ID);

            Product::find($product_id)->delete();
            ProductComponent::where(Constants::FIELD_PRODUCT_ID, $product_id)->delete();

            return Response::success("Successfully delete product");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public static function addGroup() {
        try {
            $group = array();
            foreach (Input::get() as $key => $value) {
                if ($key == Constants::ID) continue;
                Verification::check($key);
                $group[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $group[Constants::FIELD_CREATED_AT] = $time;
            $group[Constants::FIELD_UPDATED_AT] = $time;
            ProductGroup::create($group);

            return Response::success("Successfully add product group");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function editGroup() {
        try {
            $group = array();
            foreach (Input::get() as $key => $value) {
                Verification::check($key);
                if ($key == Constants::ID) continue;
                $group[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $group[Constants::FIELD_UPDATED_AT] = $time;
            ProductGroup::find(Input::get(Constants::ID))->update($group);

            return Response::success("Successfully edit product group");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function deleteGroup() {
        try {
            Verification::check(Constants::FIELD_PRODUCT_GROUP_ID);

            ProductGroup::find(Input::get(Constants::FIELD_PRODUCT_GROUP_ID))->delete();

            return Response::success("Successfully delete product group");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }
}
