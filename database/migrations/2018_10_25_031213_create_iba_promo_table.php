<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIbaPromoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iba_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_target');
            $table->string('target');
            $table->string('name');
            $table->string('description');
            $table->string('discount');
            $table->dateTime('periode');
            $table->string('promo_code');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iba_promo');
    }
}
