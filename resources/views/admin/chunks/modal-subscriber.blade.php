@extends('admin.chunks.modal')
@section("modalTitle")
    Subscriber
@endsection

@section("modalContent")
    <form action="/api/v1/subscriber/add" class="form" id="form" aria-label="subscriber">
        <input type="hidden" id="id" name="id" value="">
        <div class="row form-group">
            <label for="email" class="col-md-2 col-md-offset-1 control-label">Email</label>
            <div class="col-md-8">
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" autofocus>
            </div>
        </div>
        <div class="row form-group">
            <label for="status" class="control-label col-md-2 col-md-offset-1">Status</label>
            <div class="col-md-8">
                <select class="form-control" id="status" name="status" style="width: 100%;">
                    <option value="">Select Option</option>
                    <option value="active">Active</option>
                    <option value="inactive">Inactive</option>
                </select>
            </div>
        </div>
    </form>
@endsection
