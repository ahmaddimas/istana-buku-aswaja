<?php

namespace App\Model;

use App\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductGroup extends Model
{
    use SoftDeletes;

    protected $table = "iba_product_group";

    protected $fillable = [
        "id", "name", "created_at", "updated_at"
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function product() {
        return $this->hasMany(Product::class, Constants::FIELD_PRODUCT_GROUP_ID);
    }

    public function promo() {
        return $this->hasMany(Promo::class, Constants::FIELD_TARGET_ID);
    }
}
