<div class="modal fade" tabindex="-1" role="dialog" id="group_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Product Group</h4>
            </div>
            <div class="modal-body">
                <form action="/api/v1/product/group/add" class="form" id="form_group">
                    <input type="hidden" id="id" name="id" value="">
                    <div class="row form-group">
                        <label for="name" class="col-md-2 col-sm-2 control-label">Name</label>
                        <div class="col-md-7 col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" autofocus>
                        </div>
                        <button type="submit" data-mode="add" aria-label="group" class="btn btn-sm btn-primary col-md-2 col-sm-10 col-md-offset-0 col-sm-offset-1">Add</button>
                    </div>
                </form>
                <div class="card">
                    <div class="card-header">
                        List Product Group
                    </div>
                    <div class="card-body no-padding">
                        <table id="group-table" class="datatable table table-striped primary" width="100%">
                            <thead>
                            <tr>
                                <th style="width: 10px !important;">ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
