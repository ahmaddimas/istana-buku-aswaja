<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{asset('flat-admin/css/vendor.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('flat-admin/css/flat-admin.css')}}">

        <!-- Theme -->
        <link rel="stylesheet" type="text/css" href="{{asset('flat-admin/css/theme/blue-sky.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('flat-admin/css/theme/blue.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('flat-admin/css/theme/red.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('flat-admin/css/theme/yellow.css')}}">
        <style>
            .form-control {
                margin-bottom: 15px;
            }
            .form-extra {
                padding-top: 15px;
            }
            .control-label {
                padding-top: 7px;
            }
            .swal-overlay{
                z-index: 300000;
            }
            .swal-modal {
                z-index: 300001;
            }
        </style>
        @yield('css')

    </head>

    <body>
        <div class="app app-default">

            @include('admin.layouts.sidebar')

            <div class="app-container">
        
                @include('admin.layouts.header')
                @include('admin.layouts.float-button')
                
                @include('admin.layouts.success-errors')
                @yield('content')
                @include('admin.layouts.footer')

            </div>
        </div>
      
        <script type="text/javascript" src="{{asset('flat-admin/js/vendor.js')}}"></script>
        <script type="text/javascript" src="{{asset('flat-admin/js/app.js')}}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="http://www.datejs.com/build/date.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
        <script !src="">
            $(document).ready(function () {
                $('.datatable').DataTable().destroy();
                const form_label = $('#form').attr('aria-label');
                $('#form').parents('.modal').find('button[data-mode]').attr('aria-label', form_label);
            });
        </script>
        @yield('js')
    </body>

</html>
