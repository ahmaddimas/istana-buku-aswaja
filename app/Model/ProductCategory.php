<?php

namespace App\Model;

use App\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use SoftDeletes;

    protected $table = "iba_product_category";

    protected $fillable = [
        "id", "id_product", "id_category", "created_at", "updated_at"
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function product() {
        return $this->belongsTo(Product::class, Constants::FIELD_PRODUCT_ID);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, Constants::FIELD_CATEGORY_ID);
    }
}
