<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Istana Buku Aswaja</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-sm navbar-dark bg-iba fixed-top">
            <a href="#mynav" role="button" class="navbar-toggler" data-toggle="collapse" aria-expanded="false">
                <span class="navbar-toggler-icon" aria-hidden="true"></span>
            </a>
            <a href="#" class="navbar-brand"><span class="text-dark">Istana</span> Buku Aswaja</a>
            <div class="collapse navbar-collapse justify-content-end" id="mynav">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php"><i class="fa fa-home"></i>&nbsp;Home</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="single_book.php">Single Book</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="book.php">Book</a>
                  </li>
                </ul>
            </div>
        </nav>
        <div class="jumbotron jumbotron-fluid mb-auto" id="top-element">
            <div class="container">
                <h2 class="text-center"><i class="fa fa-search"></i>&nbsp;Search Book</h2><hr>
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 px-3">
                        <form action="" method="get" class="row">
                            <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                <label for="book-category" class="form-control-label">Category : </label>
                                <select class="form-control" name="category" id="book-category">
                                    <option>All</option>
                                    <option>Fiqih</option>
                                    <option>Aqidah</option>
                                </select>
                            </div>
                            <div class="form-group col-xl-5 col-lg-5 col-md-5 col-sm-5 col-12">
                                <label for="book-title">Title</label>
                                <input type="text" class="form-control" name="title" id="book-title" placeholder="Enter book title">
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <label for="">&nbsp;</label>
                                <button type="submit" class="btn btn-iba btn-block" name="search">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotron-fluid mb-auto bg-white">
            <div class="container">
                <h2 class="">Manga</h2><hr>
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12 px-3 mb-2">
                        <div class="card">
                            <div style="background: url(https://spark.adobe.com/images/landing/examples/architecture-book-cover.jpg);" class="single-book-image">
                                <img src="https://spark.adobe.com/images/landing/examples/architecture-book-cover.jpg" alt="background">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 px-3 mb-2">
                        <div class="row">
                            <div class="col-md-4 order-md-2 mb-3">
                                <h4 class="text-iba text-center">Rp 500.000</h4><hr>
                                <button type="button" class="btn btn-iba btn-block"><i class="fa fa-usd"></i>&nbsp;Buy</button>
                                <button type="button" class="btn btn-outline-iba btn-block"><i class="fa fa-shopping-cart"></i> &nbsp; Add to Cart</button>
                            </div>
                            <div class="col-md-8 order-md-1">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 mb-3">
                                            <div class="row">
                                                <div class="col"><i class="fa fa-check"></i> &nbsp; Available</div>
                                                <div class="col">: 4</div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 mb-3">
                                            <div class="row">
                                                <div class="col"><i class="fa fa-tag"></i> &nbsp; Category</div>
                                                <div class="col">: Fiqih</div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 mb-3">
                                            <div class="row">
                                                <div class="col"><i class="fa fa-shopping-bag"></i> &nbsp; Sold</div>
                                                <div class="col">: 4</div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs row" role="tablist">
                                        <div class="col px-0 text-center">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#description" id="description-tab" role="tab" data-toggle="tab" aria-controls="description" aria-expanded="true">Desciption</a>
                                            </li>
                                        </div>
                                        <div class="col px-0 text-center">
                                            <li class="nav-item">
                                                <a class="nav-link" href="#reviews" id="reviews-tab" role="tab" data-toggle="tab" aria-controls="reviews" aria-expanded="true">Reviews</a>
                                            </li>
                                        </div>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade show active pt-3" id="description" aria-labelledby="description-tab">
                                            <p>
                                                Pengarang : Mangaka-san <br>
                                                Penerbit : JUMP <br>
                                                Resensi : <br>
                                            </p>
                                            <p>A hat is a head covering. It can be worn for protection against the elements, ceremonial reasons, religious reasons, safety, or as a fashion accessory.</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade pt-3" id="reviews" aria-labelledby="reviews-tab">
                                            <div class="media">
                                                <img class="mr-3" src="https://www.quackit.com/pix/samples/9s.jpg" alt="Sample photo">
                                                <div class="media-body">
                                                    <h6><b class="text-iba">Saitama</b></h6>
                                                    <small><i>22 September 2017</i></small>
                                                    <p>Bukunya kereng banget</p>
                                                </div>
                                            </div>
                                            <div class="media">
                                                <img class="mr-3" src="https://www.quackit.com/pix/samples/9s.jpg" alt="Sample photo">
                                                <div class="media-body">
                                                    <h6><b class="text-iba">Saitama</b></h6>
                                                    <small><i>22 September 2017</i></small>
                                                    <p>Bukunya kereng banget</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotron-fluid my-auto">
            <div class="container">
                <h2 class="text-center"><i class="fa fa-book"></i>&nbsp;Related Books</h2><hr>
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 px-3 mb-2">
                        <div class="card">
                            <div style="background: url(https://marketplace.canva.com/MACXC0twKgo/1/0/thumbnail_large/canva-green-and-pink-science-fiction-book-cover-MACXC0twKgo.jpg) center center;" class="book-card-image">
                                <div class="book-card-image-overlay d-flex align-items-center justify-content-center">
                                    <div class="text-center">
                                        <button class="btn btn-circle-xs btn-outline-iba px-0 py-0">
                                            <i class="fa fa-heart"></i>
                                        </button>
                                        <button class="btn btn-circle-xs btn-outline-light px-0 py-0">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">Manga</h4><hr>
                                <h6 class="card-subtitle">Rp 500.000</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotron-fluid my-auto bg-iba text-white">
            <div class="container">
                <h2 class="text-center"><i class="fa fa-envelope-o"></i>&nbsp;Subscribe for Updates</h2><hr class="bg-white">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 px-3 mb-2">
                        <form action="" method="post">
                          <div class="form-group row">
                            <label for="newslatter" class="col-form-label col-lg-3">Email address</label>
                            <div class="col-lg-9">
                                <input type="email" class="form-control" id="newslatter" placeholder="Enter valid email" required>
                            </div>
                        </div>
                          <button type="submit" class="btn btn-outline-light btn-block">Subscribe Now &nbsp; <i class="fa fa-send"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotron-fluid mb-auto py-5 bg-white">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-3">
                        <h4>Istana Buku Aswaja</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ullamcorper, nunc sit amet interdum mattis.</p>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
                        <h4>Follow Us &nbsp; <i class="fa fa-rss"></i></h4>
                        <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-facebook"></i></a>
                        <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-whatsapp"></i></a>
                        <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-instagram"></i></a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <h4>Our Location &nbsp; <i class="fa fa-map-marker"></i></h4>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.2139604052813!2d112.65676931398988!3d-7.976824681733116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd6285c5c1b44e3%3A0xf6c889ac7452dc3a!2sSekolah+Menengah+Kejuruan+Telkom+Malang!5e0!3m2!1sid!2sid!4v1505499319294" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <footer class="jumbotron jumbotron-fluid mb-auto py-3 bg-iba text-white">
            <div class="container-fluid text-center">
                <h6>&copy; Copyright. All Right Reserved 2017</h6>
            </div>
        </footer>
        <!-- load javascript files -->
        <script src="js/jquery.js" charset="utf-8"></script>
        <script src="js/jquery.easing.1.3.min.js" charset="utf-8"></script>
        <script src="js/jquery.easing.compatibility.js" charset="utf-8"></script>
        <script src="js/popper.min.js" charset="utf-8"></script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            $('#top-element').css('margin-top', $('nav.navbar').height());
            function findBook() {
                $('html, body').stop().animate({
                    scrollTop: $('#list-book').offset().top - $('nav.navbar').height()
                }, 2000, 'easeInOutExpo');
            }
        </script>
    </body>
</html>
