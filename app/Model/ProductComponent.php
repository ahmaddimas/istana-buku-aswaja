<?php

namespace App\Model;

use App\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductComponent extends Model
{
    use SoftDeletes;

    protected $table = "iba_product_component";

    protected $fillable = [
        "id", "id_product", "name", "description", "created_at", "updated_at"
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function product() {
        return $this->belongsTo(Product::class, Constants::FIELD_PRODUCT_ID);
    }
}
