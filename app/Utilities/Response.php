<?php
/**
 * Created by ahmad.
 * Date: 10/25/18
 * Time: 8:24 PM
 */

namespace App\Utilities;


class Response
{
    public static function success($data) {
        return json_encode([Constants::STATUS => Constants::STATUS_SUCCESS_CODE, Constants::DATA => $data]);
    }

    public static function failed($message){
        return json_encode(array(Constants::STATUS=>Constants::STATUS_FAILED_CODE,Constants::MESSAGE=>$message));
    }
}
