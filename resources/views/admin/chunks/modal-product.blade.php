@extends('admin.chunks.modal')
@section("modalTitle")
    Product
@endsection

@section("modalContent")
    <form action="/api/v1/product/add" class="form" id="form" aria-label="product">
        <input type="hidden" id="id" name="id" value="">
        <div class="row form-group">
            <label for="name" class="col-md-2 col-md-offset-1 control-label">Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" autofocus>
            </div>
        </div>
        <div class="row form-group">
            <label for="group" class="control-label col-md-2 col-md-offset-1">Product Group</label>
            <div class="col-md-8">
                <select class="form-control" id="group" name="id_product_group" style="width: 100%;"></select>
            </div>
        </div>
        <div class="row form-group">
            <label for="category" class="control-label col-md-2 col-md-offset-1">Product Category</label>
            <div class="col-md-8">
                <select class="form-control" id="category" name="id_category" style="width: 100%;"></select>
            </div>
        </div>
        <div class="row form-group">
            <label for="stock" class="control-label col-md-2 col-md-offset-1">Stock</label>
            <div class="col-md-8">
                <input type="number" class="form-control" id="stock" name="stock" placeholder="Stock Product" autofocus>
            </div>
        </div>
        <div class="row form-group">
            <label for="price" class="control-label col-md-2 col-md-offset-1">Price</label>
            <div class="col-md-8">
                <input type="number" class="form-control" id="price" name="price" placeholder="Price Product" autofocus>
            </div>
        </div>
        <div class="row form-group">
            <label for="weight" class="control-label col-md-2 col-md-offset-1">Weight</label>
            <div class="col-md-8">
                <input type="number" class="form-control" id="weight" name="weight" placeholder="Weight Product" autofocus>
            </div>
        </div>
        <div class="form-extra text-center" data-label="component-group">
            <div class="form-wrapper">
                <div class="form-item row hidden" data-label="component" aria-readonly="true">
                    <div class="col-md-2 col-md-offset-1" data-label="component-name">
                        <input type="text" class="form-control" placeholder="Label" autofocus>
                    </div>
                    <div class="col-md-7" data-label="component-desc">
                        <input type="text" class="form-control" placeholder="Description" autofocus>
                    </div>
                    <button role="remove-component" class="btn btn-sm btn-danger col-md-1"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            <button role="add-component" class="btn btn-sm btn-primary" >Add Column</button>
        </div>
    </form>
@endsection
