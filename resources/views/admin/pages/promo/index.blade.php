@extends('admin.layouts.app')

@section('title', 'Promo')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <button role="add" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp; Add</button>
                </div>
                <div class="card-body no-padding">
                    <table id="main-table" class="datatable table table-striped primary" width="100%">
                        <thead>
                            <tr>
                                <th style="width: 10px !important;">ID</th>
                                <th>Target</th>
                                <th>Target Name</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Discount</th>
                                <th>Periode</th>
                                <th>Promo Code</th>
                                <th>Status</th>
                                <th style="width: 25% !important;">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Target</th>
                                <th>Target Name</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Discount</th>
                                <th>Periode</th>
                                <th>Promo Code</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @component('admin.chunks.modal-promo')
    @endcomponent
@endsection

@section('js')
    <script>
        $(document).ready(function () {

            /*===========================================
                DataTable
            ===========================================*/
            $('#main-table').DataTable({
                "dom": '<"top"fl<"clear">>rt<"bottom"ip<"clear">>',
                "oLanguage": {
                    "sSearch": "",
                    "sLengthMenu": "_MENU_"
                },
                "initComplete": function initComplete(settings, json) {
                    $('div.dataTables_filter input').attr('placeholder', 'Search...');
                },
                "ajax": {
                    "url": "/api/v1/promo/get_table",
                    "type": "POST",
                    'beforeSend': function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    }
                },
                "processing": true,
                "serverSide": true,
                "columns": [{
                    "data": 'id'
                }, {
                    "data": 'target'
                }, {
                    "data": 'data.name'
                }, {
                    "data": 'name'
                }, {
                    "data": 'description'
                }, {
                    "data": 'discount'
                }, {
                    "data": 'periode'
                }, {
                    "data": 'promo_code'
                }, {
                    "data": 'status'
                }, {
                    "render": function (data, type, row) {
                        return "<center>" +
                            "<button onclick='window.location.href=\"/customer/" + row.id + "\"' class='btn btn-sm btn-info'>" +
                            "<i class='fa text-white fa-eye'></i>" +
                            "</button>&nbsp;" +
                            "<button class='btn btn-sm btn-primary' role='edit' onclick='setFormData(" + JSON.stringify(row) + ")'>" +
                            "<i class='fa fa-pencil'></i>" +
                            "</button>&nbsp;" +
                            "<button class='btn btn-sm btn-danger' data-mode='delete' aria-label='promo' data-content='" + row.id + "'>" +
                            "<i class='fa fa-trash'></i>" +
                            "</button>" +
                            "</center>";
                    }
                }]
            });

            /*=======================================
                Event
            =======================================*/
            $(document).on('click', 'button[role]', function (e) {
                let role = $(e.target).attr('role');
                if (role === 'add') {
                    $("#form").find("#id_target").empty();
                }
                if (role === 'add' || role === 'edit') {
                    let action = role;
                    var mode = action === 'edit' ? 'add' : 'edit';
                    $('#form_modal').find('button[aria-label="promo"][data-mode="' + mode + '"]').attr('data-mode', action);
                    $('#form_modal').find('.modal-title').text(action.concat(' promo').toUpperCase());
                    $('#form_modal').modal('show');
                }
                e.preventDefault();
            });
            $(document).on('click', 'button[type="reset"]', function (e) {
                $('form').trigger('reset');
                e.preventDefault();
            });
            $(document).on('change', '#form #target', function (e) {
                const target = $('#form').find('#target').val();
                const segment = target === 'group' ? ['group'] : [];
                getList('form', 'id_target', 'product', segment, {'{{\App\Utilities\Constants::FIELD_TARGET}}': target});
            });
            $(document).on('click', 'button[data-mode][aria-label]', function (e) {
                let mode = $(e.target).attr('data-mode');
                let label = $(e.target).attr('aria-label');

                if (label === 'promo') {
                    var data = mode === 'delete' ? {'{{\App\Utilities\Constants::FIELD_PROMO_ID}}': $(this).attr('data-content')} : $('#form').serialize();
                }

                if (mode === 'delete') {
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this file",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((confirm) => {
                        if (!confirm) {
                            return false;
                        } else {
                            triggerForm(label, mode, data);
                        }
                    });
                } else {
                    triggerForm(label, mode, data);
                }

                e.preventDefault();
                return false;
            });
            $(document).on('hide.bs.modal', '.modal', function (e) {
                $('form').trigger('reset');
                $('.select2').val(null).trigger('change');
            });
        });

        function setFormData(data) {
            $('#form').find('#id').val(data.id);
            $('#form').find('#target').val(data.target).trigger('change');

            if ($('#form').find('#id_target').find('option').length <= 1) {
                const segment = data.target === 'group' ? ['group'] : [];
                getList('form', 'id_target', 'product', segment, {'{{\App\Utilities\Constants::FIELD_TARGET}}': data.target});
            }
            $('#form').find('#id_target').val(data.data.id);
            $('#form').find('#name').val(data.name);
            $('#form').find('#description').val(data.description);
            $('#form').find('#discount').val(data.discount);
            $('#form').find('#periode').val(data.periode);
            $('#form').find('#promo_code').val(data.promo_code);
            $('#form').find('#status').val(data.status);
        }

        getList = (form, element, target, segment = [], data = null) => {
            let additional_url = "";
            if (segment.length > 0) {
                additional_url = segment.map((el) => {
                    return '/' + el;
                });
            }
            $("#" + form).find("#" + element).empty();
            $.ajax({
                url: '/api/v1/' + target + additional_url + '/get',
                type: 'POST',
                async: false,
                data: data,
                beforeSend: function (request) {
                    request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (res) {
                    $("#" + form).find("#" + element).append('<option value="">Select Option</option>');
                    if (res.data.length > 0) {
                        $.each(res.data, function (index, value) {
                            var newOption = new Option(value.name, value.id);
                            $("#" + form).find("#" + element).append(newOption);
                        });
                    }
                }
            });
        };

        function triggerForm(target, action, data, callback = null) {
            var close = false;

            $.ajax({
                url: '/api/v1/'+ target +'/'+ action,
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        close = true;
                        swal('Success', res.data, 'success', {buttons: false, timer: 1000});
                        $('#main-table').DataTable().ajax.reload();
                    } else {
                        swal('Failed', res.message, 'error', {buttons: false, timer: 2000});
                    }
                },
                failed: function (res) {
                    res = JSON.parse(res);
                    swal('Error', res.message, 'error', {buttons: false, timer: 2000});
                },
                complete: function () {
                    if (callback !== null) {
                        callback();
                    }
                    if (action !== 'delete' && close) {
                        $('#form_modal').modal('hide');
                    }
                }
            });
            return false;
        }
    </script>
@endsection
