<?php
/**
 * Created by ahmad.
 * Date: 10/29/18
 * Time: 10:57 AM
 */

namespace App\Http\Controllers;


use App\Exceptions\Verification;
use App\Model\Subscriber;
use App\Utilities\Constants;
use App\Utilities\Response;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\Facades\DataTables;

class SubscriberController extends Controller
{
    public function getSubscriber() {
        try {
            return Response::success(Subscriber::all());
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function getTableSubscriber() {
        try {
            return DataTables::eloquent(Subscriber::query())->make(true);
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function findSubscriber($email) {
        try {
            $result = Subscriber::where(Constants::FIELD_EMAIL, $email)->first();
            return Response::success($result);
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILED);
        }
    }

    public static function addSubscriber() {
        try {
            $subscriber = array();
            foreach (Input::get() as $key => $value) {
                if ($key === Constants::FIELD_ID) continue;
                Verification::check($key);
                $subscriber[$key] = $value;
            }
            if (!array_key_exists(Constants::FIELD_STATUS, $subscriber)) {
                $subscriber[Constants::FIELD_STATUS] = Constants::STATUS_ACTIVE;
            }
            $t = new SubscriberController();
            $exist = json_decode($t->findSubscriber($subscriber[Constants::FIELD_EMAIL]), TRUE);
            if (!$exist[Constants::STATUS] || ($exist[Constants::STATUS] && count($exist[Constants::DATA]) > 0)) {
                return Response::failed(Constants::STATUS_IS_EXIST);
            }
            $time = date('Y-m-d H:i:s');
            $subscriber[Constants::FIELD_CREATED_AT] = $time;
            $subscriber[Constants::FIELD_UPDATED_AT] = $time;
            Subscriber::create($subscriber);

            return Response::success('Successfully add subscriber');
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }

    public function editSubscriber() {
        try {
            $subscriber = array();
            foreach (Input::get() as $key => $value) {
                Verification::check($key);
                if ($key === Constants::FIELD_ID) continue;
                $subscriber[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $subscriber[Constants::FIELD_UPDATED_AT] = $time;
            Subscriber::find(Input::get(Constants::FIELD_ID))->update($subscriber);

            return Response::success('Successfully update subscriber');
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILED);
        }
    }

    public function deleteSubscriber() {
        try {
            Verification::check(Constants::FIELD_SUBSCRIBER_ID);
            Subscriber::find(Input::get(Constants::FIELD_SUBSCRIBER_ID))->delete();

            return Response::success('Successfully delete subscriber');
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILED);
        }
    }
}
