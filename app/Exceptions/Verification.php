<?php
/**
 * Created by ahmad.
 * Date: 10/25/18
 * Time: 10:01 PM
 */

namespace App\Exceptions;


use App\Utilities\Constants;
use Illuminate\Support\Facades\Input;

class Verification extends \Exception
{
    public static function check($key,$message = null){
        if(Input::get($key) ==null){
            if($message==null)
                throw new Verification($key . " param required.");
            else
                throw new Verification(str_replace("%key%", $key, $message));
        }
        return Constants::STATUS_SUCCESS;
    }

}
