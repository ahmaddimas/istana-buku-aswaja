<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @yield('css')
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-iba fixed-top">
        <a href="#mynav" role="button" class="navbar-toggler" data-toggle="collapse" aria-expanded="false">
            <span class="navbar-toggler-icon" aria-hidden="true"></span>
        </a>
        <a href="#" class="navbar-brand"><span class="text-dark">Istana</span> Buku Aswaja</a>
        <div class="collapse navbar-collapse justify-content-end" id="mynav">
            <form action="" role="search" class="form-inline mr-lg-5 w-50">
                <div class="form-group has-feedback" style="width: 100%;">
                    <i class="fa fa-search form-control-feedback"></i>
                    <input type="text" class="form-control" name="search" id="search" placeholder="Search..." style="width: 100%;">
                </div>
            </form>
            <ul class="navbar-nav ml-lg-5">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php"><i class="fa fa-home"></i>&nbsp;Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="single_book.php">Single Book</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="book.php">Book</a>
                </li>
            </ul>
        </div>
    </nav>

    @yield('content')

    <div class="jumbotron jumbotron-fluid my-auto bg-iba text-white">
        <div class="container">
            <h2 class="text-center"><i class="fa fa-envelope-o"></i>&nbsp;Subscribe for Updates</h2><hr class="bg-white">
            <div class="row d-flex justify-content-center">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 px-3 mb-2">
                    <form action="" method="post">
                        <div class="form-group row">
                            <label for="newslatter" class="col-form-label col-lg-3">Email address</label>
                            <div class="col-lg-9">
                                <input type="email" class="form-control" id="newslatter" placeholder="Enter valid email" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-outline-light btn-block">Subscribe Now &nbsp; <i class="fa fa-send"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron jumbotron-fluid mb-auto py-5 bg-white">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-3">
                    <h4>Istana Buku Aswaja</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ullamcorper, nunc sit amet interdum mattis.</p>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
                    <h4><i class="fa fa-rss"></i> Follow Us</h4>
                    <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-facebook"></i></a>
                    <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-whatsapp"></i></a>
                    <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-instagram"></i></a>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h4><i class="fa fa-map-marker"></i> Our Location</h4>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.2139604052813!2d112.65676931398988!3d-7.976824681733116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd6285c5c1b44e3%3A0xf6c889ac7452dc3a!2sSekolah+Menengah+Kejuruan+Telkom+Malang!5e0!3m2!1sid!2sid!4v1505499319294" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <footer class="jumbotron jumbotron-fluid mb-auto py-3 bg-iba text-white">
        <div class="container-fluid text-center">
            <h6>&copy; Copyright. All Right Reserved 2017</h6>
        </div>
    </footer>

    <script src="{{asset('js/jquery.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/jquery.easing.1.3.min.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/jquery.easing.compatibility.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/popper.min.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/bootstrap.min.js')}}" charset="utf-8"></script>
    <script type="text/javascript">
        // $('#top-element').css('margin-top', $('nav.navbar').height());
    </script>
    @yield('js')
</body>
</html>
