<div class="modal fade" tabindex="-1" role="dialog" id="form_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">@yield("modalTitle")</h4>
            </div>
            <div class="modal-body">
                @yield("modalContent")
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                        Close
                    </button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                    <button type="button" class="btn btn-success" data-mode="add" aria-label="">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
