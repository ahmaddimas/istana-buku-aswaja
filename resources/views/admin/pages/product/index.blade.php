@extends('admin.layouts.app')

@section('title', 'Products')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body">
                    <button role="group" class="btn btn-info"><i class="fa fa-object-group"></i>&nbsp; Product Group</button>
                    <button role="category" class="btn btn-warning"><i class="fa fa-list"></i>&nbsp; Product Category</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <button role="add" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp; Add</button>
                </div>
                <div class="card-body no-padding">
                    <table id="main-table" class="datatable table table-striped primary" width="100%">
                        <thead>
                            <tr>
                                <th style="width: 10px !important;">ID</th>
                                <th>Name</th>
                                <th style="">Group</th>
                                <th style="width: 10px !important;">Stock</th>
                                <th style="width: 20px !important;">Price</th>
                                <th style="width: 10px !important;">Weight</th>
                                <th style="width: 25% !important;">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Weight</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @component('admin.chunks.modal-product')
    @endcomponent
    @component('admin.chunks.modal-product-group')
    @endcomponent
    @component('admin.chunks.modal-category')
    @endcomponent
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            getList('form', 'group', 'product', ['group']);

            /*===========================================
                DataTable
            ===========================================*/
            $('#main-table').DataTable({
                "dom": '<"top"fl<"clear">>rt<"bottom"ip<"clear">>',
                "oLanguage": {
                    "sSearch": "",
                    "sLengthMenu": "_MENU_"
                },
                "initComplete": function initComplete(settings, json) {
                    $('div.dataTables_filter input').attr('placeholder', 'Search...');
                },
                "ajax": {
                    "url": "/api/v1/product/get_table",
                    "type": "POST",
                    'beforeSend': function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    }
                },
                "processing": true,
                "serverSide": true,
                "columns": [{
                    "data": 'id'
                }, {
                    "data": 'name'
                }, {
                    "data": 'group.name'
                }, {
                    "data": 'stock'
                }, {
                    "data": 'price'
                }, {
                    "data": 'weight'
                }, {
                    "render": function (data, type, row) {
                        return "<center>" +
                            "<button onclick='window.location.href=\"/customer/" + row.id + "\"' class='btn btn-sm btn-info'>" +
                            "<i class='fa text-white fa-eye'></i>" +
                            "</button>&nbsp;" +
                            "<button class='btn btn-sm btn-primary' role='edit' onclick='setFormData(" + JSON.stringify(row) + ")'>" +
                            "<i class='fa fa-pencil'></i>" +
                            "</button>&nbsp;" +
                            "<button class='btn btn-sm btn-danger' data-mode='delete' aria-label='product' data-content='" + row.id + "'>" +
                            "<i class='fa fa-trash'></i>" +
                            "</button>" +
                            "</center>";
                    }
                }]
            });
            $('#group-table').DataTable({
                "oLanguage": {
                    "sSearch": "",
                    "sLengthMenu": "_MENU_"
                },
                "initComplete": function initComplete(settings, json) {
                    $('div.dataTables_filter input').attr('placeholder', 'Search...');
                },
                "ajax": {
                    "url": "/api/v1/product/group/get_table",
                    "type": "POST",
                    'beforeSend': function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    }
                },
                "processing": true,
                "serverSide": true,
                "columns": [{
                    "data": 'id'
                }, {
                    "data": 'name'
                }, {
                    "render": function (data, type, row) {
                        return "<center>" +
                            "<button class='btn btn-sm btn-primary' role='edit-group' onclick='setGroupData(" + JSON.stringify(row) + ")'>" +
                            "<i class='fa fa-pencil'></i>" +
                            "</button>&nbsp;" +
                            "<button class='btn btn-sm btn-danger' data-mode='delete' aria-label='group' data-content='" + row.id + "'>" +
                            "<i class='fa fa-trash'></i>" +
                            "</button>" +
                            "</center>";
                    }
                }]
            });
            $('#category-table').DataTable({
                "oLanguage": {
                    "sSearch": "",
                    "sLengthMenu": "_MENU_"
                },
                "initComplete": function initComplete(settings, json) {
                    $('div.dataTables_filter input').attr('placeholder', 'Search...');
                },
                "ajax": {
                    "url": "/api/v1/category/get_table",
                    "type": "POST",
                    'beforeSend': function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    }
                },
                "processing": true,
                "serverSide": true,
                "columns": [{
                    "data": 'id'
                }, {
                    "data": 'name'
                }, {
                    "data": 'group.name'
                }, {
                    "render": function (data, type, row) {
                        return "<center>" +
                            "<button class='btn btn-sm btn-primary' role='edit-category' onclick='setCategoryData(" + JSON.stringify(row) + ")'>" +
                            "<i class='fa fa-pencil'></i>" +
                            "</button>&nbsp;" +
                            "<button class='btn btn-sm btn-danger' data-mode='delete' aria-label='category' data-content='" + row.id + "'>" +
                            "<i class='fa fa-trash'></i>" +
                            "</button>" +
                            "</center>";
                    }
                }]
            });

            /*=======================================
                Event
            =======================================*/
            $(document).on('click', 'button[role]', function (e) {
                let role = $(e.target).attr('role');
                if (role === 'remove-component')
                    $(e.target).parents('[data-label="component"]').remove();
                if (role === 'add-component') {
                    let component = $('[data-label="component"][aria-readonly="true"]').clone().removeAttr('aria-readonly').removeClass('hidden');
                    component.find('[data-label="component-name"] input').attr('name', 'componentName[]');
                    component.find('[data-label="component-desc"] input').attr('name', 'componentDesc[]');
                    $('.form-wrapper').append(component);
                }
                if (role === 'group') {
                    $('#group_modal').modal('show');
                }
                if (role === 'edit-group') {
                    $('#form_group').find('button[data-mode="add"]').attr('data-mode', 'edit').text('Edit');
                }
                if (role === 'category') {
                    getList('form_category', 'group', 'product', ['group']);
                    $('#category_modal').modal('show');
                }
                if (role === 'edit-category') {
                    $('#form_category').find('button[data-mode="add"]').attr('data-mode', 'edit').text('Edit');
                }
                if (role === 'add' || role === 'edit') {
                    if (role === 'add') {
                        $("#form").find("#category").empty();
                    }
                    let action = role;
                    var mode = action === 'edit' ? 'add' : 'edit';
                    $('#form_modal').find('button[aria-label="product"][data-mode="' + mode + '"]').attr('data-mode', action);
                    $('#form_modal').find('.modal-title').text(action.concat(' product').toUpperCase());
                    $('#form_modal').modal('show');
                }
                e.preventDefault();
            });
            $(document).on('click', 'button[type="reset"]', function (e) {
                $('form').trigger('reset');
                e.preventDefault();
            });
            $(document).on('change', '#form #group', function (e) {
                getList('form', 'category', 'category', [], {'{{\App\Utilities\Constants::FIELD_PRODUCT_GROUP_ID}}': $('#form').find('#group').val()});
            });
            $(document).on('click', 'button[data-mode][aria-label]', function (e) {
                let mode = $(e.target).attr('data-mode');
                let label = $(e.target).attr('aria-label');
                let callback = null;

                if (label === 'product') {
                    var data = mode === 'delete' ? {'{{\App\Utilities\Constants::FIELD_PRODUCT_ID}}': $(this).attr('data-content')} : $('#form').serialize();
                }
                if (label === 'group') {
                    var data = mode === 'delete' ? {'{{\App\Utilities\Constants::FIELD_PRODUCT_GROUP_ID}}': $(this).attr('data-content')} : $('#form_group').serialize();
                    callback = () => {
                        $('#form_group').trigger('reset');
                        getList('form', 'group', 'product', ['group']);
                    };
                    if (mode === 'edit') {
                        callback = () => {
                            $('#form_group').trigger('reset');
                            $('#form_group').find('button[data-mode="edit"]').attr('data-mode', 'add').text('Add');
                            getList('form', 'group', 'product', ['group']);
                        }
                    }
                    label = 'product/'+ label;
                }
                if (label === 'category') {
                    var data = mode === 'delete' ? {'{{\App\Utilities\Constants::FIELD_CATEGORY_ID}}': $(this).attr('data-content')} : $('#form_category').serialize();
                    callback = () => {
                        $('#form_category').trigger('reset');
                        getList('form', 'category', 'category', [], {'{{\App\Utilities\Constants::FIELD_PRODUCT_GROUP_ID}}': $('#form').find('#group').val()});
                    };
                    if (mode === 'edit') {
                        callback = () => {
                            $('#form_category').trigger('reset');
                            $('#form_category').find('button[data-mode="edit"]').attr('data-mode', 'add').text('Add');
                            getList('form', 'category', 'category', [], {'{{\App\Utilities\Constants::FIELD_PRODUCT_GROUP_ID}}': $('#form').find('#group').val()});
                        }
                    }
                }

                if (mode === 'delete') {
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this file",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((confirm) => {
                        if (!confirm) {
                            return false;
                        } else {
                            triggerForm(label, mode, data);
                        }
                    });
                } else {
                    triggerForm(label, mode, data, callback);
                }

                e.preventDefault();
                return false;
            });
            $(document).on('hide.bs.modal', '.modal', function (e) {
                $('form').trigger('reset');
                $('.select2').val(null).trigger('change');
            });
        });

        function setFormData(data) {
            var price = data.price.replace('.', '');
            $('#form').find('#id').val(data.id);
            $('#form').find('#name').val(data.name);
            $('#form').find('#group').val(data.group.id).trigger('change');
            if ($('#form').find('#category').find('option').length <= 1) {
                getList('form', 'category', 'category', [], {'{{\App\Utilities\Constants::FIELD_PRODUCT_GROUP_ID}}': $('#form').find('#group').val()});
            }
            $('#form').find('#category').val(data.category[0].id);
            $('#form').find('#stock').val(data.stock);
            $('#form').find('#price').val(price);
            $('#form').find('#weight').val(data.weight);
        }

        function setGroupData(data) {
            $('#form_group').find('#id').val(data.id);
            $('#form_group').find('#name').val(data.name);
        }

        function setCategoryData(data) {
            $('#form_category').find('#id').val(data.id);
            $('#form_category').find('#name').val(data.name);
            $('#form_category').find('#group').val(data.group.id);
        }

        getList = (form, element, target, segment = [], data = null) => {
            let additional_url = "";
            if (segment.length > 0) {
                additional_url = segment.map((el) => {
                    return '/' + el;
                });
            }
            $("#" + form).find("#" + element).empty();
            $.ajax({
                url: '/api/v1/' + target + additional_url + '/get',
                type: 'POST',
                async: false,
                data: data,
                beforeSend: function (request) {
                    request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (res) {
                    $("#" + form).find("#" + element).append('<option value="">Select Option</option>');
                    if (res.data.length > 0) {
                        $.each(res.data, function (index, value) {
                            var newOption = new Option(value.name, value.id);
                            $("#" + form).find("#" + element).append(newOption);
                        });
                    }
                }
            });
        };

        function triggerForm(target, action, data, callback = null) {
            var close = false;
            let table = target === 'product' ? 'main-table' : target === 'category' ? 'category-table' : 'group-table';

            $.ajax({
                url: '/api/v1/'+ target +'/'+ action,
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        close = true;
                        swal('Success', res.data, 'success', {buttons: false, timer: 1000});
                        $('#'+ table).DataTable().ajax.reload();
                    } else {
                        swal('Failed', res.message, 'error', {buttons: false, timer: 2000});
                    }
                },
                failed: function (res) {
                    res = JSON.parse(res);
                    swal('Error', res.message, 'error', {buttons: false, timer: 2000});
                },
                complete: function () {
                    if (callback !== null) {
                        callback();
                    }
                    if (target === 'product' && action !== 'delete' && close) {
                        $('#form_modal').modal('hide');
                    }
                }
            });
            return false;
        }
    </script>
@endsection
