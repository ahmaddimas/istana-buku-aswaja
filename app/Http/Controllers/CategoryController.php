<?php
/**
 * Created by ahmad.
 * Date: 10/27/18
 * Time: 8:27 PM
 */

namespace App\Http\Controllers;


use App\Exceptions\Verification;
use App\Model\Category;
use App\Utilities\Constants;
use App\Utilities\Response;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function getCategory()
    {
        try {
            $rows = Category::all();
            $input = Input::get();
            if (array_key_exists(Constants::FIELD_PRODUCT_GROUP_ID, $input)) {
                $condition = [Constants::FIELD_PRODUCT_GROUP_ID => Input::get(Constants::FIELD_PRODUCT_GROUP_ID)];
                $rows = Category::where($condition)->get();
            }
            $result = array();

            if ($rows) {
                $result = $rows->map(function ($item) {
                    $temp = array();
                    foreach ($item->toArray() as $key => $value) {
                        if ($key !== Constants::FIELD_PRODUCT_GROUP_ID) {
                            $temp[$key] = $value;
                        }
                    }
                    $temp['group'] = $item->group->toArray();
                    return $temp;
                });
            }
            return Response::success($result);
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function getTableCategory()
    {
        try {
            $rows = Category::all();
            $input = Input::get();
            if (array_key_exists(Constants::FIELD_PRODUCT_GROUP_ID, $input)) {
                $condition = [Constants::FIELD_PRODUCT_GROUP_ID => Input::get(Constants::FIELD_PRODUCT_GROUP_ID)];
                $rows = Category::where($condition)->get();
            }
            $result = array();

            if ($rows) {
                $result = $rows->map(function ($item) {
                    $temp = array();
                    foreach ($item->toArray() as $key => $value) {
                        if ($key !== Constants::FIELD_PRODUCT_GROUP_ID) {
                            $temp[$key] = $value;
                        }
                    }
                    $temp['group'] = $item->group->toArray();
                    return $temp;
                });
            }
            return DataTables::collection($result)->make(true);
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public static function addCategory()
    {
        try {
            $category = array();
            foreach (Input::get() as $key => $value) {
                if ($key == Constants::ID) continue;
                Verification::check($key);
                $category[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $category[Constants::FIELD_CREATED_AT] = $time;
            $category[Constants::FIELD_UPDATED_AT] = $time;
            Category::create($category);

            return Response::success("Successfully add category");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function editCategory()
    {
        try {
            $category = array();
            foreach (Input::get() as $key => $value) {
                Verification::check($key);
                if ($key == Constants::ID) continue;
                $category[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $category[Constants::FIELD_UPDATED_AT] = $time;
            Category::find(Input::get(Constants::ID))->update($category);

            return Response::success("Successfully edit category");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function deleteCategory()
    {
        try {
            Verification::check(Constants::FIELD_CATEGORY_ID);

            Category::find(Input::get(Constants::FIELD_CATEGORY_ID))->delete();

            return Response::success("Successfully delete category");
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }
}
