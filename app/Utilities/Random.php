<?php
/**
 * Created by ahmad.
 * Date: 10/29/18
 * Time: 3:43 PM
 */

namespace App\Utilities;


class Random
{
    public static function generate($length = 6) {
        $chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ023456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $key = '' ;

        while ($i <= $length) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $key = $key . $tmp;
            $i++;
        }

        return $key;
    }
}
