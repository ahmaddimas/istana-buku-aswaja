<?php

namespace App\Model;

use App\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = "iba_product";

    protected $fillable = [
        "id", "id_product_group", "name", "stock", "price", "weight", "created_at", "updated_at"
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function group() {
        return $this->belongsTo(ProductGroup::class, Constants::FIELD_PRODUCT_GROUP_ID);
    }

    public function component() {
        return $this->hasMany(ProductComponent::class, Constants::FIELD_PRODUCT_ID);
    }

    public function category() {
        return $this->belongsToMany(Category::class, 'iba_product_category', Constants::FIELD_PRODUCT_ID, Constants::FIELD_CATEGORY_ID);
    }

    public function review() {
        return $this->hasMany(ProductReview::class, Constants::FIELD_PRODUCT_ID);
    }

    public function promo() {
        return $this->hasMany(Promo::class, Constants::FIELD_TARGET_ID);
    }
}
