<?php

namespace App\Model;

use App\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promo extends Model
{
    use SoftDeletes;

    protected $table = "iba_promo";

    protected $fillable = [
        "id", "id_target", "target", "name", "description", "discount", "periode", "promo_code", "status", "created_at", "updated_at"
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function product() {
        return $this->belongsTo(Product::class, Constants::FIELD_TARGET_ID);
    }

    public function group() {
        return $this->belongsTo(ProductGroup::class, Constants::FIELD_TARGET_ID);
    }
}
