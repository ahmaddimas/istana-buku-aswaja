<?php
/**
 * Created by ahmad.
 * Date: 10/25/18
 * Time: 1:58 PM
 */


/*=======================
        View
=========================*/
Route::get('/admin', function () { return view('admin/home'); });
Route::get('/admin/product', function () { return view('admin/pages/product/index'); });
Route::get('/admin/promo', function () { return view('admin/pages/promo/index'); });
Route::get('/admin/subscriber', function () { return view('admin/pages/subscriber/index'); });

/*=======================
        DataTables
=========================*/
Route::any('/api/v1/product/get_table', 'ProductController@getTableProduct');
Route::any('/api/v1/product/group/get_table', 'ProductController@getTableGroup');
Route::any('/api/v1/category/get_table', 'CategoryController@getTableCategory');
Route::any('/api/v1/promo/get_table', 'PromoController@getTablePromo');
Route::any('/api/v1/subscriber/get_table', 'SubscriberController@getTableSubscriber');


/*=======================
        Product
=========================*/
Route::any('/api/v1/product/get', 'ProductController@getProduct');
Route::post('/api/v1/product/add', 'ProductController@addProduct');
Route::post('/api/v1/product/edit', 'ProductController@editProduct');
Route::post('/api/v1/product/delete', 'ProductController@deleteProduct');

/*=======================
    Product Group
=========================*/
Route::any('/api/v1/product/group/get', 'ProductController@getGroup');
Route::post('/api/v1/product/group/add', function(){return \App\Http\Controllers\ProductController::addGroup();});
Route::post('/api/v1/product/group/edit', 'ProductController@editGroup');
Route::post('/api/v1/product/group/delete', 'ProductController@deleteGroup');

/*=======================
        Category
=========================*/
Route::any('/api/v1/category/get', 'CategoryController@getCategory');
Route::post('/api/v1/category/add', function(){return \App\Http\Controllers\CategoryController::addCategory();});
Route::post('/api/v1/category/edit', 'CategoryController@editCategory');
Route::post('/api/v1/category/delete', 'CategoryController@deleteCategory');

/*=======================
        Promo
=========================*/
Route::any('/api/v1/promo/get', 'PromoController@getPromo');
Route::post('/api/v1/promo/add', function(){return \App\Http\Controllers\PromoController::addPromo();});
Route::post('/api/v1/promo/edit', 'PromoController@editPromo');
Route::post('/api/v1/promo/delete', 'PromoController@deletePromo');

/*=======================
        Subscriber
=========================*/
Route::any('/api/v1/subscriber/get', 'SubscriberController@getSubscriber');
Route::post('/api/v1/subscriber/add', function(){return \App\Http\Controllers\SubscriberController::addSubscriber();});
Route::post('/api/v1/subscriber/edit', 'SubscriberController@editSubscriber');
Route::post('/api/v1/subscriber/delete', 'SubscriberController@deleteSubscriber');
