<?php
/**
 * Created by ahmad.
 * Date: 10/29/18
 * Time: 10:54 AM
 */

namespace App\Http\Controllers;


use App\Exceptions\Verification;
use App\Model\Promo;
use App\Utilities\Constants;
use App\Utilities\Random;
use App\Utilities\Response;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\Facades\DataTables;

class PromoController extends Controller
{
    public function __construct()
    {
//        $this->middleware()
    }

    public function getPromo() {
        try {
            $rows = Promo::all();
            /*$input = Input::get();
            if (array_key_exists(Constants::FIELD_TARGET, $input)) {
                $condition = [Constants::FIELD_TARGET => Input::get(Constants::FIELD_TARGET)];
                $rows = Promo::where($condition)->get();
            }*/
            $result = array();

            if ($rows) {
                $result = $rows->map(function ($item) {
                    $temp = array();
                    foreach ($item->toArray() as $key => $value) {
                        if ($key !== Constants::FIELD_TARGET_ID) {
                            $temp[$key] = $value;
                        }
                    }
                    if ($item->target === 'product')
                        $temp['data'] = $item->product->toArray();
                    if ($item->target === 'group')
                        $temp['data'] = $item->group->toArray();
                    return $temp;
                });
            }
            return Response::success($result);
        } catch (Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public function getTablePromo() {
        try {
            $rows = Promo::all();
            /*$input = Input::get();
            if (array_key_exists(Constants::FIELD_TARGET, $input)) {
                $condition = [Constants::FIELD_TARGET => Input::get(Constants::FIELD_TARGET)];
                $rows = Promo::where($condition)->get();
            }*/
            $result = array();

            if ($rows) {
                $result = $rows->map(function ($item) {
                    $temp = array();
                    foreach ($item->toArray() as $key => $value) {
                        if ($key !== Constants::FIELD_TARGET_ID) {
                            $temp[$key] = $value;
                        }
                    }
                    if ($item->target === 'product')
                        $temp['data'] = $item->product->toArray();
                    if ($item->target === 'group')
                        $temp['data'] = $item->group->toArray();
                    return $temp;
                });
            }
            return DataTables::collection($result)->make(true);
        } catch (Exception $e) {
            return Response::failed($e->getMessage());
            return Response::failed(Constants::STATUS_FAILURE);
        }
    }

    public static function addPromo() {
        try {
            $promo = array();
            foreach (Input::get() as $key => $value) {
                if ($key === Constants::ID) continue;
                Verification::check($key);
                $promo[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $promo[Constants::FIELD_PROMO_CODE] = Random::generate();
            $promo[Constants::FIELD_CREATED_AT] = $time;
            $promo[Constants::FIELD_UPDATED_AT] = $time;
            Promo::create($promo);

            return Response::success('Successfully add promo');
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
        }
    }

    public function editPromo() {
        try {
            $promo = array();
            foreach (Input::get() as $key => $value) {
                Verification::check($key);
                if ($key === Constants::ID) continue;
                $promo[$key] = $value;
            }
            $time = date('Y-m-d H:i:s');
            $promo[Constants::FIELD_UPDATED_AT] = $time;
            Promo::find(Input::get(Constants::ID))->update($promo);

            return Response::success('Successfully update promo');
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILED);
        }
    }

    public function deletePromo() {
        try {
            Verification::check(Constants::FIELD_PROMO_ID);
            Promo::find(Input::get(Constants::FIELD_PROMO_ID))->delete();

            return Response::success('Successfully delete promo');
        } catch (Verification $v) {
            return Response::failed($v->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::STATUS_FAILED);
        }
    }
}
