@extends('admin.chunks.modal')
@section("modalTitle")
    Promo
@endsection

@section("modalContent")
    <form action="/api/v1/promo/add" class="form" id="form" aria-label="promo">
        <input type="hidden" id="id" name="id" value="">
        <div class="row form-group">
            <label for="target" class="control-label col-md-2 col-md-offset-1">Target</label>
            <div class="col-md-8">
                <select class="form-control" id="target" name="target" style="width: 100%;">
                    <option value="">Select Option</option>
                    <option value="group">Product Group</option>
                    <option value="product">Product</option>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <label for="id_target" class="control-label col-md-2 col-md-offset-1">Target Name</label>
            <div class="col-md-8">
                <select class="form-control" id="id_target" name="id_target" style="width: 100%;"></select>
            </div>
        </div>
        <div class="row form-group">
            <label for="name" class="col-md-2 col-md-offset-1 control-label">Name</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" autofocus>
            </div>
        </div>
        <div class="row form-group">
            <label for="description" class="col-md-2 col-md-offset-1 control-label">Description</label>
            <div class="col-md-8">
                <textarea class="form-control" id="description" name="description" placeholder="Description" autofocus></textarea>
            </div>
        </div>
        <div class="row form-group">
            <label for="discount" class="col-md-2 col-md-offset-1 control-label">Discount</label>
            <div class="col-md-8">
                <input type="number" class="form-control" id="discount" name="discount" max="100" placeholder="Discount" autofocus>
            </div>
        </div>
        <div class="row form-group">
            <label for="periode" class="col-md-2 col-md-offset-1 control-label">Periode</label>
            <div class="col-md-8">
                <input type="date" class="form-control" id="periode" name="periode" placeholder="Periode" autofocus>
            </div>
        </div>
        <div class="row form-group">
            <label for="status" class="control-label col-md-2 col-md-offset-1">Status</label>
            <div class="col-md-8">
                <select class="form-control" id="status" name="status" style="width: 100%;">
                    <option value="">Select Option</option>
                    <option value="active">Active</option>
                    <option value="inactive">Inactive</option>
                </select>
            </div>
        </div>
    </form>
@endsection
