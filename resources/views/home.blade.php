@extends('layouts.app')

@section('title')
    Home
@endsection

@section('content')
    <div class="intro-wrapper parallax" id="top-element" style="background: url(images/img1.jpg) center center;">
        {{--<div class="carousel slide" data-ride="carousel" id="slider">
            <ol class="carousel-indicators">
                <li data-target="#slider" data-slide-to="0" class="active"></li>
                <li data-target="#slider" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner fixed-carousel">
                <div class="carousel-item active" style="background: url(images/img1.jpg) center center;"></div>
                <div class="carousel-item" style="background: url(images/img2.jpg) center center;"></div>
            </div>
        </div>--}}
        <div class="intro-caption-wrapper d-flex align-items-center">
            <div class="container text-white text-center">
                <h1>Istana Buku Aswaja</h1>
                <hr class="hr-style">
                <div class="row d-flex justify-content-center">
                    <p class="col-md-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ullamcorper, nunc sit amet interdum mattis.</p>
                </div>
                <button onclick="findBook()" class="btn btn-outline-iba">Find Your Book <i class="fa fa-angle-down"></i></button>
            </div>
        </div>
    </div>
    {{--<div class="jumbotron jumbotron-fluid mb-auto bg-iba" id="intro">
        <div class="container">
            <div class="row text-center">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <button type="button" class="btn btn-circle-lg btn-light text-iba mb-3"><i class="fa fa-car"></i></button>
                    <h3>Free</h3>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <button type="button" class="btn btn-circle-lg btn-light text-iba mb-3"><i class="fa fa-car"></i></button>
                    <h3>Free</h3>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <button type="button" class="btn btn-circle-lg btn-light text-iba mb-3"><i class="fa fa-car"></i></button>
                    <h3>Free</h3>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <button type="button" class="btn btn-circle-lg btn-light text-iba mb-3"><i class="fa fa-car"></i></button>
                    <h3>Free</h3>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="jumbotron jumbotron-fluid mb-auto bg-white" id="list-book">
        <div class="container">
            <h2 class="text-center"><i class="fa fa-search"></i>&nbsp;Search Books</h2><hr>
            <div class="row d-flex justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 px-3">
                    <form action="" method="get" class="row">
                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <label for="book-category" class="form-control-label">Category : </label>
                            <select class="form-control" name="category" id="book-category">
                                <option>All</option>
                                <option>Fiqih</option>
                                <option>Aqidah</option>
                            </select>
                        </div>
                        <div class="form-group col-xl-5 col-lg-5 col-md-5 col-sm-5 col-12">
                            <label for="book-title">Title</label>
                            <input type="text" class="form-control" name="title" id="book-title" placeholder="Enter book title">
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                            <label for="">&nbsp;</label>
                            <button type="submit" class="btn btn-iba btn-block" name="search">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron jumbotron-fluid mb-auto">
        <div class="container">
            <h2 class="text-center"><i class="fa fa-book"></i>&nbsp;Latest Books</h2><hr>
            <div class="row">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 px-3 mb-2">
                    <div class="card">
                        <div style="background: url(https://spark.adobe.com/images/landing/examples/architecture-book-cover.jpg);" class="book-card-image">
                            <div class="book-card-image-overlay d-flex align-items-center justify-content-center">
                                <div class="text-center">
                                    <button class="btn btn-circle-xs btn-outline-iba px-0 py-0">
                                        <i class="fa fa-heart"></i>
                                    </button>
                                    <button class="btn btn-circle-xs btn-outline-light px-0 py-0">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">Manga</h6><hr>
                            <h6 class="card-subtitle text-iba">Rp 500.000</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron jumbotron-fluid my-auto bg-white">
        <div class="container">
            <h2 class="text-center"><i class="fa fa-book"></i>&nbsp;Popular Books</h2><hr>
            <div class="row">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 px-3 mb-2">
                    <div class="card">
                        <div style="background: url(https://marketplace.canva.com/MACXC0twKgo/1/0/thumbnail_large/canva-green-and-pink-science-fiction-book-cover-MACXC0twKgo.jpg);" class="book-card-image">
                            <div class="book-card-image-overlay d-flex align-items-center justify-content-center">
                                <div class="text-center">
                                    <button class="btn btn-circle-xs btn-outline-iba px-0 py-0">
                                        <i class="fa fa-heart"></i>
                                    </button>
                                    <button class="btn btn-circle-xs btn-outline-light px-0 py-0">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">Manga</h6><hr>
                            <h6 class="card-subtitle text-iba">Rp 500.000</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron jumbotron-fluid my-auto">
        <div class="container">
            <h2 class="text-center"><i class="fa fa-book"></i>&nbsp;Most Viewed</h2><hr>
            <div class="row">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 px-3 mb-2">
                    <div class="card">
                        <div style="background: url(https://about.canva.com/wp-content/uploads/sites/3/2015/01/creative_bookcover.png);" class="book-card-image">
                            <div class="book-card-image-overlay d-flex align-items-center justify-content-center">
                                <div class="text-center">
                                    <button class="btn btn-circle-xs btn-outline-iba px-0 py-0">
                                        <i class="fa fa-heart"></i>
                                    </button>
                                    <button class="btn btn-circle-xs btn-outline-light px-0 py-0">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">Manga ka-san</h6><hr>
                            <h6 class="card-subtitle text-iba">Rp 500.000</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script !src="">
        var scrollTop = $(window).scrollTop();
        $('.parallax').css('background-position', 'center '+ scrollTop +'px');
        $(window).scroll(function () {
            scrollTop = $(this).scrollTop();
            $('.parallax').css('background-position', 'center '+ (scrollTop * 0.5) +'px');
        });
        function findBook() {
            $('html, body').stop().animate({
                scrollTop: $('#list-book').offset().top - $('nav.navbar').height()
            }, 2000, 'easeInOutExpo');
        }
    </script>
@endsection
