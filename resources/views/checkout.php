<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Istana Buku Aswaja</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-sm navbar-dark bg-iba fixed-top">
            <a href="#mynav" role="button" class="navbar-toggler" data-toggle="collapse" aria-expanded="false">
                <span class="navbar-toggler-icon" aria-hidden="true"></span>
            </a>
            <a href="#" class="navbar-brand"><span class="text-dark">Istana</span> Buku Aswaja</a>
            <div class="collapse navbar-collapse justify-content-end" id="mynav">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php"><i class="fa fa-home"></i>&nbsp;Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="single_book.php">Single Book</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="book.php">Book</a>
                  </li>
                </ul>
            </div>
        </nav>
        <div class="jumbotron jumbotron-fluid mb-auto" id="top-element">
            <div class="container-fluid">
                <div class="row px-5">
                    <div class="col-md-7 mb-3">
                        <div class="card mb-3 border-0">
                            <div class="card-header bg-iba text-white">
                                <h5 class="my-auto">Buyer Details</h5>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Full Name</label>
                                    <input type="text" class="form-control" id="" name="">
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label for="">Email</label>
                                        <input type="email" class="form-control" id="" name="">
                                    </div>
                                    <div class="col">
                                        <label for="">Phone Number</label>
                                        <input type="number" class="form-control" min="0" id="" name="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Full Address</label>
                                    <textarea class="form-control" name="" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card border-0">
                            <div class="card-header bg-iba text-white">
                                <h5 class="my-auto">Shopping List</h5>
                            </div>
                            <div class="card-body">
                                <div class="media">
                                    <img class="mr-3 img-shopping-list" src="https://spark.adobe.com/images/landing/examples/architecture-book-cover.jpg" alt="Book">
                                    <div class="media-body">
                                        <h6><b>Saitama from One Punch-Man</b></h6>
                                        <span class="text-iba">Rp 500.000</span>
                                        <div class="input-group quantity-input mt-2">
                                            <span class="input-group-btn"><button type="button" class="btn btn-secondary">-</button></span>
                                            <input type="number" class="form-control" name="" value="1" min="1">
                                            <span class="input-group-btn"><button type="button" class="btn btn-secondary">+</button></span>
                                        </div>
                                    </div>
                                    <button class="btn btn-outline-light align-self-center text-secondary"><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="form-group">
                                    <label for="">Note (optional)</label>
                                    <textarea class="form-control" name="" rows="5" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="card border-0">
                            <div class="card-header bg-iba text-white">
                                <h5 class="my-auto">Shopping Summary</h5>
                            </div>
                            <div class="card-body">
                                <a data-toggle="collapse" href="#voucher-code" aria-expanded="false" aria-controls="voucher-code">Have a voucher code?</a>
                                <div class="collapse" id="voucher-code">
                                    <div class="input-group mt-2">
                                        <input type="text" class="form-control" name="" required>
                                        <span class="input-group-btn"><button type="button" class="btn btn-secondary">Use voucher code</button></span>
                                    </div>
                                </div>
                                <div class="my-3">
                                    Total : <span class="pull-right">Rp 500.000</span><br>
                                    Shipping costs : <span class="pull-right">Rp 20.000</span><hr>
                                    Total price : <span class="pull-right">Rp 520.000</span><br>
                                </div>
                                <button type="button" class="btn btn-block btn-iba">Verify</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotron-fluid my-auto bg-iba text-white">
            <div class="container">
                <h2 class="text-center"><i class="fa fa-envelope-o"></i>&nbsp;Subscribe for Updates</h2><hr class="bg-white">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 px-3 mb-2">
                        <form action="" method="post">
                          <div class="form-group row">
                            <label for="newslatter" class="col-form-label col-lg-3">Email address</label>
                            <div class="col-lg-9">
                                <input type="email" class="form-control" id="newslatter" placeholder="Enter valid email" required>
                            </div>
                        </div>
                          <button type="submit" class="btn btn-outline-light btn-block">Subscribe Now &nbsp; <i class="fa fa-send"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotron-fluid mb-auto py-5 bg-white">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-3">
                        <h4>Istana Buku Aswaja</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ullamcorper, nunc sit amet interdum mattis.</p>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-4">
                        <h4>Follow Us &nbsp; <i class="fa fa-rss"></i></h4>
                        <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-facebook"></i></a>
                        <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-whatsapp"></i></a>
                        <a href="#" role="button" class="btn btn-circle-xs btn-outline-iba"><i class="fa fa-instagram"></i></a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <h4>Our Location &nbsp; <i class="fa fa-map-marker"></i></h4>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.2139604052813!2d112.65676931398988!3d-7.976824681733116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd6285c5c1b44e3%3A0xf6c889ac7452dc3a!2sSekolah+Menengah+Kejuruan+Telkom+Malang!5e0!3m2!1sid!2sid!4v1505499319294" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <footer class="jumbotron jumbotron-fluid mb-auto py-3 bg-iba text-white">
            <div class="container-fluid text-center">
                <h6>&copy; Copyright. All Right Reserved 2017</h6>
            </div>
        </footer>
        <!-- load javascript files -->
        <script src="js/jquery.js" charset="utf-8"></script>
        <script src="js/jquery.easing.1.3.min.js" charset="utf-8"></script>
        <script src="js/jquery.easing.compatibility.js" charset="utf-8"></script>
        <script src="js/popper.min.js" charset="utf-8"></script>
        <script src="js/bootstrap.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            $('#top-element').css('margin-top', $('nav.navbar').height());
            $(".quantity-input button").on("click", function() {
                var $button = $(this);
                var oldValue = $button.parent().parent().find("input").val();

                if ($button.text() == "+") {
                    var newVal = parseFloat(oldValue) + 1;
                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 1) {
                        var newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                $button.parent().parent().find("input").val(newVal);
            });
        </script>
    </body>
</html>
