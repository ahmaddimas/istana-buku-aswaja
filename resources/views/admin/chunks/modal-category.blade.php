<div class="modal fade" tabindex="-1" role="dialog" id="category_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Category</h4>
            </div>
            <div class="modal-body">
                <form action="/api/v1/category/add" class="form row" id="form_category">
                    <input type="hidden" id="id" name="id" value="">
                    <div class="row form-group form-inline col-md-6 col-sm-12">
                        <label for="group" class="col-md-2 control-label">Group</label>
                        <div class="col-md-10">
                            <select name="id_product_group" id="group" class="form-control"
                                    style="width: 100%;"></select>
                        </div>
                    </div>
                    <div class="row form-group form-inline col-md-6 col-sm-12">
                        <label for="name" class="col-md-2 control-label">Name</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="name" name="name" style="width: 100%;"
                                   placeholder="Name" autofocus>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" data-mode="add" aria-label="category" class="btn btn-primary">Add</button>
                    </div>
                </form>
                <div class="card">
                    <div class="card-header">
                        List Category
                    </div>
                    <div class="card-body no-padding">
                        <table id="category-table" class="datatable table table-striped primary" width="100%">
                            <thead>
                            <tr>
                                <th style="width: 10px !important;">ID</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
