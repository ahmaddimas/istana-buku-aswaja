<?php
/**
 * Created by ahmad.
 * Date: 10/25/18
 * Time: 11:16 AM
 */

namespace App\Utilities;


class Constants
{
    /* field database */
    const FIELD_ID = "id";
    const FIELD_PRODUCT_COMPONENT_ID = "id_product_component";
    const FIELD_PRODUCT_GROUP_ID = "id_product_group";
    const FIELD_PRODUCT_ID = "id_product";
    const FIELD_CATEGORY_ID = "id_category";
    const FIELD_SUBSCRIBER_ID = "id_subscriber";
    const FIELD_PROMO_ID = "id_promo";
    const FIELD_TARGET_ID = "id_target";
    const FIELD_NAME = "name";
    const FIELD_EMAIL = "email";
    const FIELD_TARGET = "target";
    const FIELD_PRICE = "price";
    const FIELD_STOCK = "stock";
    const FIELD_DESCRIPTION = "description";
    const FIELD_PROMO_CODE = "promo_code";
    const FIELD_PROMO_PERIODE = "periode";
    const FIELD_PROMO_DISCOUNT = "discount";
    const FIELD_STATUS = "status";
    const FIELD_CREATED_AT = "created_at";
    const FIELD_UPDATED_AT = "updated_at";

    /* public identifier */
    const ID = "id";
    const COMPONENT_NAME = "componentName";
    const COMPONENT_DESC = "componentDesc";
    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";

    const DATA = "data";
    const MESSAGE = "message";

    /* status success */
    const STATUS = "status";
    const STATUS_SUCCESS = "success";
    const STATUS_SUCCESS_CODE = 1;
    const STATUS_SUCCESS_OK = "ok";
    const STATUS_SUCCESS_OK_CODE = 200;
    const STATUS_SUCCESS_CREATED = "created";
    const STATUS_SUCCESS_CREATED_CODE = 201;
    const STATUS_SUCCESS_ACCEPTED = "accepted";
    const STATUS_SUCCESS_ACCEPTED_CODE = 202;
    const STATUS_SUCCESS_NO_CONTENT = "no content";
    const STATUS_SUCCESS_NO_CONTENT_CODE = 204;
    /* status failed */
    const STATUS_FAILED = "failed";
    const STATUS_FAILED_CODE = 0;
    const STATUS_FAILURE = "failure";
    const STATUS_FAILURE_CODE = 500;
    const STATUS_IS_EXIST = "Data already exist";
    const STATUS_NOT_FOUND = "Data not found";
}
