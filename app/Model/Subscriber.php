<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriber extends Model
{
    use SoftDeletes;

    protected $table = "iba_subscriber";

    protected $fillable = [
        "id", "email", "status", "created_at", "updated_at"
    ];

    protected $hidden = [];

    protected $dates = ['deleted_at'];
}
